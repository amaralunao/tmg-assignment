filter {
	if [application][name] == "haproxy" and [product][type] == "api" {
		mutate {
			add_field => [ "[index][name]", "haproxy-api" ]
		}

		# Fix naming error in Filebeat
		if [product][name] == "Content API" {
			mutate {
				replace => { "[product][name]" => "Kiosk" }
			}
		}

		# Add geoip data based on the IP address.
		if ("" in [client_ip]) {
			geoip {
				source => "client_ip"
				target => "geoip"
				fields => [ "city_name", "continent_code", "country_name", "location", "postal_code", "region_name", "timezone" ]
			}
		}

 		mutate {
			# Split http_request into request_path and query_string
			split => { "http_request" => "?" }

			# Split the request header into seperate fields to attempt to retrieve the
			# domain name, the user agent, the x_app_auth_token and the x_user_auth_token
			split => { "captured_request_headers" => "|" }
		}

		#############################################
		# START processing captured_request_headers #
		#############################################

		if ("" in [captured_request_headers][0]) {
			# Store request header host
			mutate {
				add_field => [ "[request_header][host]", "%{[captured_request_headers][0]}" ]
			}

			if ("" in [captured_request_headers][1]) {
				# Store request header useragent
				useragent {
					source => "[captured_request_headers][1]"
					target => "[request_header][useragent]"
				}

				# Make sure the following filters are only attempted on the products that make use of auth tokens.
				if [product][name] in ["Triple-A API", "Kiosk", "Data API"] {
					if ("" in [captured_request_headers][3]) {
						mutate {
							add_field => [ "x_app_auth_token", "%{[captured_request_headers][3]}" ]
						}

						if [product][name] == "Triple-A API" {
							translate {
								field => "x_app_auth_token"
								destination => "x_app_auth_name"
								# dictionary_path => "/tmp/triplea_apps.csv"
								dictionary => [	"113422e19ddea51116258d56d6a73b1c8992bbf5a14ecc2c44dc3577527e15b8", "Accounts Web",
									"8fd128f136a5bf130165a6ffaed4fcae9d39b3df38d8a43ac5b8ffca735a3e94", "Allenta Paywall Client",
									"c649041a94c41f451cc9943a97e389022cb3f85ffa4af55942918e481f85ddc9", "Autovisie app (pinch)",
									"563b095aa95ddbe18e3106373af7ee133b555d9a644564500d9532ec7fe131a5", "Blendle app",
									"22d29eaa1875698c66e72fc94fade8732585556d52969b42131ffe61a89e33d1", "Blendle Test app",
									"730fef36c1aa2c6530129b2b54054cfc6eafd88a57351c0c83ef7b1579ed0006", "DFT app",
									"ae1f7418f78117d63814b334b1196cd8b62878187088420bf8700f39c05e759f", "EredivisiePro",
									"235e77940cbc6a4c415e0d6cc0d92221fddc148016de409b4aabc0d01199db08", "HMC",
									"5fd9c85b1da435f4d0fd3d801cecb2c84106d775d3de04a288f780abe8da70fa", "HMC_HD",
									"0e441a9cd8279783b9408e5189e9dd00dc41e2552e430df0b45b6bc7fdf2f1e9", "HMC_LD",
									"5bdb2d42f9c9619ad3d42eaf8125a6616605df215b782f3e34ae3f49c831b0f3", "HMC_NHD",
									"61da0ecbead1538c55680204b6d17fdc6806c9b8f21515bdedf77dd11efa2c32", "HMC_YC",
									"7030b093eda3068c8226b49458897e99b4255a7bf8e8037fa952072ea3abb7ea", "Inkijkvenster App",
									"3667aabe908e302c531932e1014e61cf7994ff540e7339c3d44e9c130848c6f1", "Kiosk Content API",
									"d49c3d2fd4d136cf4d03a7741262aaebbf0fc1167a433db8f4524e7a85634f9a", "Metro App (IBM)",
									"13ecc869d162cd270cf4cb9d3d8d824ae8a9f42758a7b29fd36e032f9cac5b1b", "Metro App (React Native)",
									"27e2a52d3e415d12ef507b089c7312c3412eaac0b8b4a0e556cf3849cc9e546e", "Metro Web",
									"11bd73150984f55f37c1d1d59aab4e8473d21b747f52bcfe06a8145cf5fa9e40", "Mijnmedia App",
									"1528ddf77cf7b9b78ec8be62126d042d37ff4cfac3b0d225fdce6d3c62d0ad1c", "Mobile Telegraaf Test App",
									"5a7e7d6853686dd3095de52d377368525799004e895877503b97dd25b402a0d7", "Monitoring",
									"c5432d4016801eb5f20b9e5641f9e610bab81f47e0b73a723b58a390327e904a", "Native Login Android",
									"7c61bc8482255f4549e6231ed10e8eb54e40ff9dbfbb8a596993757f3e2aaf0b", "Native Login IOS",
									"ee207420f5858ced1dae6b8fb550474479f9775b76e8a19d26d377f6e43861ef", "Pine PenTest 1",
									"18e14f44322ea52b6b846851581f981f5a617d7cbc8c8ab37bda5824d799ca9d", "Pine PenTest 2",
									"a74beb8ce18e94ffd22929500dc016bcd3037f13964455fe0d530d52c7fb44cc", "POC - Andre",
									"ae4f9fa6cc8d4856ee82385db1f21d0a11cb01f9f0347c23f46c539bc03e656d", "Prive app (pinch)",
									"a973a9e18a8a0b01422661faf92eb6dde99ffb6716883542d2886dd730b5448c", "Prive Digital Magazine",
									"c78ded2c4a62df03da0f3fbd0daa75e82de6cc9a7b1581d0befb9b84cb467759", "SAML",
									"6decaee130f9c4c2ee20479f0e943c48a5834b2d9601a5fb9a2e0726c8c2ab62", "Selenium Test",
									"01bee8fa95f43bb00cd44e537a8ad78b95614c9db17c471441b23e4b22448cdc", "Telegraaf 2.0",
									"ee61b7e1d75ee444ca0e6ed059a96bedb64968a4c13ae135ffdb2970b9d85d11", "Telegraaf 2.0 App Android",
									"fc5cc0be215b7557b1846264f31a42cafe8ce4710e35f2c9a39c7bdb7f9f907f", "Telegraaf App Andriod",
									"c156a523f19c729ada65c52eeba2709bd34d27026640ed6c1c0c712081098445", "Telegraaf App iOS",
									"841b2a88bb9511c104ffaab9919ee4ffe96868fc717b055584f7e64834633eed", "Telegraaf AppleTV App (Pinch)",
									"71270328820d51ffa8f0d06a33f2d3373f618ed3f13861a192dada4b6f376086", "Telegraaf Archief App (Pinch)",
									"a16a6a43cb0b43c18bf81aeb9bd55735376fc321bdb4fcf08558c965c1969783", "Telegraaf Archief App (Solidam)",
									"5b3231fccf986ef40ebdff5db0361e3bab1e011e5005cb058fdd407642c23848", "Telegraaf Avond App (Pinch)",
									"82b4ba4ba18989eb711ffa1ea480ba8b8979a8b97d3975da9f4ce44615a88891", "Telegraaf Krant App (Pinch)",
									"c682cf63e589e4eb4b7bfff176cbfa29ac52b62f2d142d7cacfc6173c237a2a4", "Telegraaf Krant App (Solidam)",
									"3f83c5a926f494b63dcd9354b57c3be7920802a983c531b152789b7cf0ef678c", "Telegraaf Replica App Android",
									"f91066521ab4678183a698e9d9a88ceba355f0a8a7a359de139cf8c5c26524c4", "Telegraaf Replica App IOS",
									"ae41b109bebdf2fcb7f0f328842898f1c438786f520385f250d0b0bce053babc", "Telegraaf Sandboxed",
									"5b616be5b14556de35522ab6221ce1e88a27b4aad4ed3fe93775635b7c208074", "Telegraaf Test App (Pinch)",
									"b5594101b7ac5a17817adabeee4f2336c7c488553c5df5488a033db5be3d4815", "Telegraaf Test App (Solidam)",
									"35bd86dd7983d26fd024e37adec70f3f88da832770182101fe88b68c367b7a00", "UKI",
									"2fe7190206b76e6a0e2a8429b2c23bf632f048e67f56346c30a90ce7b1034e61", "VraagHugo App",
									"951b4d63373ea952925e76a4e52cc1c174157e2d602183b76c84a6fdae8b460c", "Vrouw Glossy app (pinch)",
									"9d2fc2c9698d5dce364a43a7ae4300a7379402a3be29b176c1eac5ba7ea3dd1b", "Vrouw Website" ]
								fallback => "%{x_app_auth_token}"
							}
						}

						if [product][name] == "Kiosk" {
							translate {
								field => "x_app_auth_token"
								destination => "x_app_auth_name"
								# dictionary_path => "/tmp/triplea_apps.csv"
								dictionary => [	"RctxKAUB4X3ZEttpMAnzDEqORCJL3XZSmHVHTb2Gdmg=", "concernbi",
									"4WMbji/YAQ+3bp1DIKrvMbAXdKrqs1Oj3LMhSN0jwys=", "datalab",
									"DvYWHb3NMjjJRy0MbleF5JPdjlG0EQUdDlVaUHg7pUg=", "demo",
									"53Nd/xXSBR9LxVAYzK+okGf+6Fwkj/WPxeqG+uuKgXg=", "devin",
									"xiA21IibBwV37FNGlHyYOdK+wEHCIuRIPqAuammofAA=", "gooieneemlander.nl",
									"GnhXoAt92Jsf8ljF2Yt6629AQHNNTy7EaCost1636ys=", "haarlemsdagblad.nl",
									"i+CzQARRimP2kSsg1NPcWKYOy3NmWfiDjTDZh3N4qjU=", "hmc-algemeen",
									"Fkzaj5lxX0vg6b3XUcsMrYvGamVgXBHSoRwNYXNsF6o=", "ijmuidercourant.nl",
									"iAMrv3EyUtoolabq1cJK39dB1acH8cZwkdQ9m4NM8l0=", "jacob",
									"aenX567uZ4tJhdAoKea3ftiPuXsF+YoqhEtNLg9GgDg=", "jeroen",
									"siyvoi31cOOqN/2x8c60fb1ltwhWVeFZfw8kgCDbWUQ=", "leidschdagblad.nl",
									"2woLd6TlCh3JhfMLH7Vt9GvpXGcXEA9HDkqVGq4t1u8=", "nagios",
									"p9zkIAMs5A2SOAIEboQvmWHOQmih/EouTMkGWbGdYpc=", "newsletter",
									"6EtXuvlhF5EQ+dA37AaGiQo/cKo628LD5etTWxetZb8=", "noordhollandsdagblad.nl",
									"4H/JgA5mXCFS0wUi+V6y9AjYM5tw6HSdYkXS6NwHsmE=", "prive.nl",
									"sMokVhSGP/FF0MY1OUs/fhvBP+xYNiJlQSu9hqn/mX4=", "smoke_test",
									"JXNgpKYv6do2FarT3EnB2skKsqK6Owob3/iRxlS5Nkw=", "telegraaf",
									"S78gwYxAjghHGzLOnyi8U0Axl/OfUZkxW47kRWl00AA=", "telegraaf-desking-tool",
									"ugKFrqr4NwGrsyvb68A6MeXynHd/Ml4HApBY+3LEYTc=", "telegraaf-telesport",
									"06LfPBedZdYf3iOjEvBzcouLcBXZHlRSQwAe9uP+63U=", "telesport",
									"zNBiNdiRfL1d2UCnNlHwXQA3lrfqZ8D8JEehOrCnqKg=", "datalab",
									"DvYWHb3NMjjJRy0MbleF5JPdjlG0EQUdDlVaUHg7pUg=", "demo",
									"53Nd/xXSBR9LxVAYzK+okGf+6Fwkj/WPxeqG+uuKgXg=", "devin",
									"3G30nCuuQa39jVNEgyrMdw4mKnbHhnORAmkOmsXH0EE=", "editor (TMP)",
									"GDTVV59lXbu2EXbsuZWuIQzu0QGRxhbrHh8uDcVs+uI=", "franc",
									"K9T6qQx6B7FOdlz412FV0IBPwt737ZQLw6uzWaZGA/o=", "gooieneemlander.nl",
									"dcL6Z56jwIcCqd22x4+RPCltz19jbCVT/yYzrblt2Y8=", "haarlemsdagblad.nl",
									"ECkpmHiBm078Ct89/ADNbaHZreNh9f+wYg5eL4MUa90=", "hmc-algemeen",
									"iTC7wIHMGQRfMFgya7GKGf10kbyTi2nYRCiBoYdMkB8=", "ijmuidercourant.nl",
									"naD5yuSH0YojAWufELMNPfUlA9QgWozSmWnXUGiLDkc=", "jacob",
									"Z8KqmLuc9aXoUtHxUF9kEt8qnLu+6ltU7qe0eFBv1D0=", "jeroen",
									"ccWb4cARvaZFuEkOuFCwM7uGOpUd1hP8qSOmE1kjlgM=", "leidschdagblad.nl",
									"PhTHYfQx2sTDQIsrlm5AbgVXP/BnmgxJB19wWLk6M6c=", "nagios",
									"ZWo3WHe8XDOhDNYS8xMK2IdY4Np7nx6kKd2L/pRFpwo=", "newsletter",
									"kWX7mcqYiwm123Tl4eiteM+M54Bk3XSa0Qvesr3dAfY=", "noordhollandsdagblad.nl",
									"m0zZpmbFweh7n1DAQ5nVlkXK0J2u8GLdSDk0cTNio5Y=", "prive.nl",
									"uqcsRFQ0aqu3NAj5OtXgzBEZ4z7GbSarAjQWoYo198E=", "smoke_test",
									"xPGX0jpmputZa88oknlnKu4kRx7gxCznsbUlK96VRjA=", "telegraaf",
									"ul9QSOnrfo4WiVngjMy+dcaO0DvNtPGd8n+0dfaOYpI=", "telegraaf-desking-tool",
									"Ynz6VRyxdHEAIlrjdC1pkj58AYMUqRmpkQxN6YjDtFs=", "telegraaf-integration-test",
									"20RAFICE3x2FxmPiFhHhuzxYoEJFbHw/k84wdASGkro=", "telegraaf-telesport",
									"yrShn8mc6iqDoucCyv3iDSlKwTzD/wx8/nkDryEsTzs=", "telegraaf-telesport (TEST)",
									"2xcgEWRo5DDssiT2s3HgZRgBvoVWLRSF364Ff4DeiZI=", "telesport" ]
								fallback => "%{x_app_auth_token}"
							}
						}

						if ("" in [captured_request_headers][4]) {
							mutate {
								add_field => [ "x_user_auth_token", "%{[captured_request_headers][4]}" ]
							}
						}
					}
				}
			}
		}
		###########################################
		# END processing captured_request_headers #
		###########################################

		#################################
		# START processing http_request #
		#################################

		if ("" in [http_request][0]) {
			# Store request path
			mutate {
				add_field => [ "request_path", "%{[http_request][0]}" ]
			}

			if ("" in [http_request][1]) {
				# Store query string
				mutate {
					add_field => [ "query_string", "%{[http_request][1]}" ]
				}

				# Split the query string into seperate fields
				kv {
					field_split => "&?"
					source => "query_string"
					target => "qs"
				}

				# transform http%3A%2F%2F to http:// etc
				urldecode {
					field => "qs"
				}
			}
		}

		###############################
		# END processing http_request #
		###############################

		mutate {
			# save diskspace
			# Temporarily keep original message and request_headers
			# remove_field => [ "http_request", "query_string", "haproxy_hour", "haproxy_milliseconds", "haproxy_minute", "haproxy_month", "haproxy_monthday", "haproxy_second", "haproxy_year", "haproxy_time", "[qs][u]", "actconn", "backend_queue", "beconn", "bytes_read", "feconn", "http_verb", "http_version", "pid", "retries", "srv_queue", "srvconn", "termination_state", "time_backend_connect", "time_backend_response", "time_duration", "time_queue", "time_request" ]
		}
	}
}