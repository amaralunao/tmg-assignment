TMG Python Dev Case
-------------------
-------------------

Requirements
============
- docker
- docker-compose
- python2/3, pip, virtualenv

Install
=======

```
$ cd app
$ virtualenv env
$ source ./env/bin/activate
$ pip install -r requirements.txt
$ python src/manage.py runserver
```

visit: http://127.0.0.1:8000/api/v1/top/articles

You see an empty list, as Elastic is not running

Start Elastic
=============

Run the ELK stack in a different terminal:

```
$ cd docker-elk
$ docker-compose up
```

wait till everything is loaded, especially filebeat and logstash. Trigger
the load of data:

```
$ cp ../haproxy.log filebeat/logs/
```

visit: http://127.0.0.1:8000/api/v1/top/articles

You will see a list of top read articles, for the last 24 hours. To see the top for 10 days:
visit: http://127.0.0.1:8000/api/v1/top/articles?timespan=10d


Assignment
==========

The current application provides the required functionality to return the top list of
articles. We want to roll it out to our production site, but a fellow engineer remarked
it would probably not handle the load. You are asked to improve the application in
such way that it can handle 10.000s of requests per second. The update time of the result
should not exceed more than ten minutes.

Make a solution in the current python app. Create a git repo from this source, and commit your solutions.
We are also interested in how you work, so show intermediate results. Share the git repo with us.

Load Testing
============

```
$ sudo npm install -g loadtest
```
```
$ loadtest -n 10000 -k http://127.0.0.1:8000/api/v1/top/articles\?timespan\=10d
```
