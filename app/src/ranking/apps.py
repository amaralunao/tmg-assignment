from django.apps import AppConfig


class RankingConfig(AppConfig):
    name = 'ranking'

    def ready(self):
        """
        Callback for app code startup.

        Gets called whenever manage.py is run, not just when the application
        as a whole starts up.

        This function can import models (locally, not at the module level) and
        access model classes, but it *must not interact with the database*.
        """
