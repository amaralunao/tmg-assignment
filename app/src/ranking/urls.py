"""
URL patterns for the rankings API.
"""

from __future__ import (
    print_function,
    unicode_literals,
    )

from django.conf.urls import url
from ranking import views

__metaclass__ = type


urlpatterns = [
    url(r'^articles$', views.ReadArticleList.as_view()),
    url(r'^tags$', views.ReadTagsList.as_view()),
    ]
