from __future__ import (
    absolute_import,
    print_function,
    unicode_literals,
)
from collections import namedtuple
from functools import partial
import json
import logging
import six
import re
from core import elastic
from core.util import get_zero_or_one
from django.conf import settings
from django.urls import resolve
from django.urls.exceptions import Resolver404
from rest_framework.exceptions import APIException
from six.moves.http_client import BAD_REQUEST


__metaclass__ = type


logger = logging.getLogger('rankings')
urllib = six.moves.urllib

default_timespan = '24h'
re_timespan = re.compile(r'^(?P<quantity>\d+)(?P<unit>\w)$')
agg_ranking = 'rankings'
agg_results = 'agg_results'
agg_details = 'detailed'


class RankingException(APIException):
    def __init__(self, detail):
        self.detail = detail
        self.status_code = BAD_REQUEST


class RankingTimeout(APIException):
    status_code = six.moves.http_client.ACCEPTED
    default_detail = 'come back for your answer later'


def get_query_string(hit):
    """
    Return a reference to the "qs" part of hit, for convenience..
    """
    try:
        return hit[agg_details]['hits']['hits'][0]['_source']['qs']
    except KeyError:
        # This is annoying to debug.  Knowing the structure of the actual
        # data can help a lot.
        logger.error("Could not find 'qs' key in Elastic hit: %s", hit)
        raise



class RankingQuery:
    """
    RankingQuery holds search parameters
    """
    _repository = None

    def __init__(self,
                 section,
                 type,
                 environment,
                 timespan,
                 field,
                 size=10,
                 page=1,
                 max_age=None,
                 ):
        """
        Create a handle to rankings matching the given criteria.

        If section is None, all sections are matched.

        If type is None, all page types are matched.

        Results are paginated in chunks of "size" results.

        The timespan is a limit to filter in time the results
        e.g. 1h, 1d, 1w
        """
        assert field in self.repository.field_mappings, "Invalid field"

        self.size = int(size)
        self.page = page
        self.section = section
        self.type = type
        self.environment = environment
        self.timespan = timespan
        self.field = field
        self.max_age = max_age
        self.product_code = 'telegraaf'
        self.index_name = 'haproxy-beacon-bus-telegraaf-*'

    @classmethod
    def from_query(cls, field, query):
        """
        :param field: Field used to aggregate on [article, tag]
        :param query: dict of parameters in the query_string
        :return: RankingQuery instance
        """
        code = query.get('product_code')
        section = query.get('section')
        type = query.get('type')
        page = int(query.get('page', 1))
        size = int(query.get('size', 10))
        timespan = query.get('timespan', default_timespan)
        environment = query.get('environment', 'prod')
        max_age = query.get('max_age')

        return cls(
            size=size,
            page=page,
            type=type,
            section=section,
            environment=environment,
            timespan=timespan,
            max_age=max_age,
            field=field)

    @property
    def repository(self):
        result = self.__class__._repository
        if not result:
            result = RankingRepository()
            self.__class__._repository = result
        return result

    def as_elastic_search(self, timespan=None, aggregated=True):
        field = self.repository.field_mappings[self.field].field_name

        terms = {
            "product.code": self.product_code,
            "application.environment": self.environment,
        }

        if self.type and self.type != 'all':
            terms["qs.hay.page.article.type"] = self.type
        time_range = timespan or 'now-%s' % (self.timespan,), None
        fltr = elastic.terms(terms) + elastic.range("@timestamp", time_range)

        if self.section and self.section != 'all':
            # Don't just match against the section field in Elastic; it's a
            # legacy field to keep Krux happy.  Instead, match against the
            # full section_array.
            #
            # This way, an article can show up in the most-read listings for
            # any of the sections in which it's published, e.g. both "sports"
            # and "darts."
            match_section = {"qs.hay.page.section_array.array": self.section}
            fltr += elastic.match(match_section)

        search = (
            elastic.Search(index=self.index_name)
            .query(elastic.Bool(filter=fltr))
            .extra(size=0)
        )
        if aggregated:
            (search.aggs
             .bucket(agg_results, 'terms', field=field, size=100)
             .metric(agg_details, 'top_hits', size=1))
        return search

    def _select_page(self, listing):
        """
        Return offset and size for the current page of listing.
        """
# TODO: Paging in this way won't cope with concurrent data changes.
        offset = (self.page - 1) * self.size
        end = offset + self.size
        return listing[offset:end]

    def get_page(self):
        data = self.repository._fetch_from_elastic(self)
        return self._select_page(data)


FieldMap = namedtuple('FieldMap', ('field_name', 'is_article'))


class RankingRepository:
    """
    Repository of most-read articles, tags, etc. based on Elastic Search.
    """

    # different filters for rankings
    field_mappings = {
        'article': FieldMap('qs.hay.page.article.title.keyword', True),
        'tag': FieldMap('qs.hay.page.article.tags.array', False),
        'id': FieldMap('qs.hay.page.article.id.keyword', True),
        'kiosk_id': FieldMap('qs.hay.page.article.kiosk_id.keyword', True),
    }

    def __init__(self,
                 request_timeout=2,
                 elastic_timeout=120):
        self.request_timeout = request_timeout
        self.elastic_timeout = elastic_timeout

    @staticmethod
    def article_from_hit(hit):
        """
        Return dict composed from a search result hit.

        :param hit: A single item in an ElasticSearch query result.
        """
        prefix = 'hay.page.article.'
        qs = get_query_string(hit).to_dict()
        return dict(
            id=qs.get(prefix + 'id'),
            title=qs.get(prefix + 'title'),
            count=hit['doc_count'],
            id_kiosk=qs.get(prefix + 'kiosk_id')
        )

    id_from_hit = kiosk_id_from_hit = escenic_id_from_hit = article_from_hit

    @staticmethod
    def tag_from_hit(hit):
        """
        Create a ReadTag from an ElasticSearch hit.
        :param hit: A single item in an ElasticSearch query result.
        """
        return dict(description=hit.key, count=hit.doc_count)


    def search_elastic(self, search):
        """
        Query rankings in Elastic Search.
        :param search: Elasticsearch-dsl.Search object
        :return: ElasticSearch response or None if failed.
        """
        cli = elastic.get_client(settings)
        try:
            params = {'request_timeout': self.elastic_timeout}
            return search.using(cli).params(**params).execute()
        except elastic.ElasticsearchException:
            logger.exception("Failed to search Elastic.")
            return None

    def _fetch_from_elastic(self, ranking_q):
        """
        Query ElasticSearch, return parsed data.
        """
        is_article = self.field_mappings[ranking_q.field].is_article
        extractor = self.article_from_hit if is_article else self.tag_from_hit
        search = ranking_q.as_elastic_search()
        response = self.search_elastic(search)
        try:
            buckets = response.aggregations[agg_results].buckets
        except (AttributeError, KeyError):
            return []

        return [extractor(hit) for hit in buckets]
