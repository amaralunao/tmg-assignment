from __future__ import (
    absolute_import,
    print_function,
    unicode_literals,
)

from ranking.repository import (
    RankingQuery,
)
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.response import Response
from rest_framework.views import APIView


__metaclass__ = type


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


class ObjectList(APIView):
    """
    General APIView descendant which uses `field` attribute of the class
    to produce the aggregation on. Alternatively, the `field` can be passed
    in as query string parameter. (It has to be one of the predefined fields
    declared by `RankingRepository.field_mappings`)
    """
    field = None

    @method_decorator(cache_page(CACHE_TTL))
    def get(self, request, version):
        field = request.GET.get('field', self.field)
        ranking_q = RankingQuery.from_query(field, request.GET)
        ranking = ranking_q.get_page()
        return Response(ranking)


class ReadArticleList(ObjectList):
    """
    List of most-read articles.
    """
    field = 'article'


class ReadTagsList(ObjectList):
    """
    List of most-read tags.
    """
    field = 'tag'
