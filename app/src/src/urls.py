from __future__ import (
    absolute_import,
    print_function,
    unicode_literals,
    )

from django.conf.urls import (
    url,
    include,
    )
from django.contrib import admin

__metaclass__ = type


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/(?P<version>v1)/top/', include('ranking.urls')),
]
