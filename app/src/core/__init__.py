"""
The ``core`` app contains models that belong to the data that is being
collected, processed and served by the Data API application.
"""
