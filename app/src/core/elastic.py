from core.util import ignore_unused
from functools import partial
from elasticsearch import (
    Elasticsearch,
    RequestsHttpConnection,
)
from elasticsearch.exceptions import ElasticsearchException
from elasticsearch_dsl import A, Q, Search
from elasticsearch_dsl.query import Bool
from os import environ

_unused = A, Bool, Search

# We import ElasticsearchException just so that other modules can import it
# from here, rather than from elasticsearch.exceptions.
ignore_unused(ElasticsearchException)


class ProxiedElasticConnection(RequestsHttpConnection):
    """
    Workaround: force Elasticsearch connection through our proxy.

    We set HTTPS_PROXY, but the Elasticsearch client bypasses it.  This was
    not a problem for the legacy in-house setup, but it is a problem now that
    the client is in the in-house data centre and the Elastic server is out
    in the cloud.

    This solution found at:
    https://stackoverflow.com/questions/32778235/elasticsearch-python-proxy
    """
    def __init__(self, *args, **kwargs):
        super(ProxiedElasticConnection, self).__init__(*args, **kwargs)

        # Set up self.session.proxies, after the connection is constructed.
        # The connection object (in the superclass code) will use it.
        if getattr(self.session, 'proxies') is None:
            self.session.proxies = {}
        if environ.get('HTTP_PROXY') is not None:
            self.session.proxies['http'] = environ['HTTP_PROXY']
        if environ.get('HTTPS_PROXY') is not None:
            self.session.proxies['https'] = environ['HTTPS_PROXY']


def get_client(settings):
    """
    Return an ElasticSearch client.
    """
    if settings.ELASTIC_CREDS is None:
        credentials = None
    else:
        credentials = (
            settings.ELASTIC_CREDS.get('username'),
            settings.ELASTIC_CREDS.get('password'),
            )
    return Elasticsearch(
        settings.ELK_STACK_URL, http_auth=credentials,
        connection_class=ProxiedElasticConnection)


def query_elastic_search(settings, index, query, params=()):
    """
    Query Elastic Search.

    :return: ElasticSearch response.
    """
    es = get_client(settings)
    return es.search(index=index, body=query, params=params)


def q_builder(q, kv, val=None):
    """
    Build a list of Q terms either from k: v or k as dict
    :param q: q-term as first parameter for Q()
    :param k: either dict for multiple terms or key in combo with v parameter
    :param v: value of q-term if k is key of q-term
    :return: list of Q() terms
    """
    if isinstance(kv, dict):
        return [q_builder(q, k, v)[0] for k, v in kv.items()]
    return [Q(q, **{kv: val})]


match_phrase = partial(q_builder, 'match_phrase')
match = partial(q_builder, 'match')
term = partial(q_builder, 'term')
bool = partial(Q, 'bool')


def range(field, limits):
    restrictions = {}
    if limits[0] is not None:
        restrictions['gte'] = limits[0]
    if limits[1] is not None:
        restrictions['lt'] = limits[1]

    return [{
        "range": {field: restrictions}
    }]


def terms(terms):
    return [{"term": {k: v}} for k, v in terms.items()]
