"""
Miscellaneous helpers.
"""
from __future__ import (
    absolute_import,
    print_function,
    unicode_literals,
    )

__metaclass__ = type


def ignore_unused(*args):
    """
    Note that variables are unused, and SHUT UP LINT CHECKERS about it.

    Pass anything you like.  This function does nothing.  It just counts as
    a "use" of your variable.
    """


def get_zero_or_one(queryset):
    """
    If queryset contains exactly one model object, return it.

    Returns None if queryset is empty.  Otherwise, raises the model class'
    MultipleObjectsReturned exception just like a regular get() would.
    """
    objects = queryset[:2]
    if len(objects) == 0:
        return None
    elif len(objects) == 1:
        return objects[0]
    else:
        raise objects[0].MultipleObjectsReturned()